package com.abreu.kotlin_clean.presentation.presenter

import com.abreu.kotlin_clean.domain.model.entity.Movie
import com.abreu.kotlin_clean.domain.use_case.MovieCase
import com.abreu.kotlin_clean.presentation.dagger.scope.PerActivity
import com.abreu.kotlin_clean.presentation.view.MovieView
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

@PerActivity
class MoviePresenter @Inject constructor(val movieCase: MovieCase) : BasePresenter() {
    var movieView: MovieView? = null

    override fun <T> setView(view: T) {
        super.setView(view)
        movieView = view as MovieView
    }

    override fun onDestroy() {
        movieCase.disposeAll()
        super.onDestroy()
        movieView = null
    }

    fun getMovies() {
        showLoading()
        movieCase.getMovies(MovieDisposableSubscriber())
    }

    private inner class MovieDisposableSubscriber : DisposableSubscriber<MutableList<Movie>>() {
        override fun onNext(movies: MutableList<Movie>?) {
            hideLoading()
            movieView?.setMovies(movies ?: mutableListOf())
        }

        override fun onError(t: Throwable?) {
            hideLoading()
        }

        override fun onComplete() {
        }

    }
}
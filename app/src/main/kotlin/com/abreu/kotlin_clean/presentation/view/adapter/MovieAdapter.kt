package com.abreu.kotlin_clean.presentation.view.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.abreu.kotlin_clean.R
import com.abreu.kotlin_clean.domain.model.entity.Movie
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_movie.view.*

class MovieAdapter(context: Context, resource: Int, movieList: MutableList<Movie>) : ArrayAdapter<Movie>(context, resource, movieList) {

    var movieList: MutableList<Movie> = mutableListOf()
    var layoutInflater: LayoutInflater? = null

    init {
        this.movieList = movieList
        this.layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val movieViewHolder: MovieViewHolder
        val movie: Movie = movieList[position]
        var view: View? = null

        if (convertView == null) {
            view = layoutInflater?.inflate(R.layout.item_movie, parent, false)
            movieViewHolder = MovieViewHolder(view)
            view?.tag = movieViewHolder
        } else {
            movieViewHolder = convertView.tag as MovieViewHolder
        }

        movieViewHolder.textViewTitle?.text = movie.title
        movieViewHolder.imageViewBackground?.loadUrl(movie.coverImageUrl)

        return view ?: convertView ?: View(context)
    }

    internal class MovieViewHolder(view: View?) {
        var textViewTitle: TextView? = null
        var imageViewBackground: ImageView? = null

        init {
            this.textViewTitle = view?.textViewTitle
            this.imageViewBackground = view?.imageViewBackground
        }
    }

    fun ImageView.loadUrl(url: String) {
        Picasso.with(context)
                .load(url)
                .centerCrop()
                .fit()
                .into(this)
    }
}
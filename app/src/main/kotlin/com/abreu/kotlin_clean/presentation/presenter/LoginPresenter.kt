package com.abreu.kotlin_clean.presentation.presenter

import com.abreu.kotlin_clean.domain.model.entity.User
import com.abreu.kotlin_clean.domain.use_case.AuthCase
import com.abreu.kotlin_clean.presentation.dagger.scope.PerActivity
import com.abreu.kotlin_clean.domain.model.request.LoginRequest
import com.abreu.kotlin_clean.presentation.view.LoginView
import com.abreu.kotlin_clean.presentation.view.activity.MovieActivity
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

@PerActivity
class LoginPresenter @Inject constructor(val authCase: AuthCase) : BasePresenter() {
    var loginView: LoginView? = null

    override fun <T> setView(view: T) {
        super.setView(view)
        loginView = view as LoginView
    }

    override fun onDestroy() {
        authCase.disposeAll()
        super.onDestroy()
        loginView = null
    }

    fun doLogin(username: String = "", password: String = "") {
        showLoading()
        authCase.doLogin(LoginRequest(username, password), LoginDisposableSubscriber())
    }

    private inner class LoginDisposableSubscriber : DisposableSubscriber<User>() {
        override fun onError(t: Throwable?) {
            hideLoading()
            showToast("onError")
        }

        override fun onNext(t: User?) {
            hideLoading()
            startActivity(MovieActivity::class.java)
        }

        override fun onComplete() {
        }

    }
}
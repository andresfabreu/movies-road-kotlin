package com.abreu.kotlin_clean.presentation.view

import android.content.Intent

interface BaseView {

    fun showToast(message: String? = "")

    fun showLoading()

    fun hideLoading()

    fun <T> startActivity(classParam: Class<T>, intent: Intent?)
}
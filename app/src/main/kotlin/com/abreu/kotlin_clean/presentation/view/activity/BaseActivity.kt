package com.abreu.kotlin_clean.presentation.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import com.abreu.kotlin_clean.presentation.AndroidApplication
import com.abreu.kotlin_clean.presentation.dagger.component.ActivityComponent
import com.abreu.kotlin_clean.presentation.dagger.component.DaggerActivityComponent
import com.abreu.kotlin_clean.presentation.dagger.module.ActivityModule
import com.abreu.kotlin_clean.presentation.view.BaseView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_progress_bar.*

abstract class BaseActivity : AppCompatActivity(), BaseView {

    protected companion object {
        lateinit var component: ActivityComponent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = DaggerActivityComponent.builder()
                .applicationComponent(AndroidApplication.component)
                .activityModule(ActivityModule(this))
                .build()

    }

    override fun showToast(message: String?) {
        Toast.makeText(this, message ?: "", Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        linearLayoutLoading?.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        linearLayoutLoading?.visibility = View.GONE
    }

    fun EditText.onTextChanged(onTextChanged: (Unit) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                onTextChanged.invoke(Unit)
            }

            override fun afterTextChanged(editable: Editable?) {
            }
        })
    }

    fun ImageView.loadUrl(url: String) {
        Picasso.with(context).load(url).into(this)
    }

    override fun <T> startActivity(classParam: Class<T>, intent: Intent?) {
        startActivity(intent ?: Intent(this, classParam))
    }
}

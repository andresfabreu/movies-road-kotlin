package com.abreu.kotlin_clean.presentation.dagger.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity
package com.abreu.kotlin_clean.presentation.view.activity

import android.os.Bundle
import com.abreu.kotlin_clean.R
import com.abreu.kotlin_clean.domain.model.entity.Movie
import com.abreu.kotlin_clean.presentation.presenter.MoviePresenter
import com.abreu.kotlin_clean.presentation.view.MovieView
import com.abreu.kotlin_clean.presentation.view.adapter.MovieAdapter
import kotlinx.android.synthetic.main.activity_movie.*
import javax.inject.Inject

class MovieActivity : BaseActivity(), MovieView {

    @Inject lateinit var moviePresenter: MoviePresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)
        component.inject(this)
        moviePresenter.setView(this)

        moviePresenter.getMovies()
    }

    override fun setMovies(movies: MutableList<Movie>) {
        listViewMovie.adapter = MovieAdapter(this, R.layout.item_movie, movies)
    }

}
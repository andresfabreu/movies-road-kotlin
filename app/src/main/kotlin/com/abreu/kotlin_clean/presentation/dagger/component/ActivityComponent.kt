package com.abreu.kotlin_clean.presentation.dagger.component

import com.abreu.kotlin_clean.presentation.dagger.module.ActivityModule
import com.abreu.kotlin_clean.presentation.dagger.scope.PerActivity
import com.abreu.kotlin_clean.presentation.view.activity.LoginActivity
import com.abreu.kotlin_clean.presentation.view.activity.MovieActivity
import dagger.Component


/**
 * A base component upon which fragment's components may depend.
 * Activity-level components should extend this component.
 *
 *
 * Subtypes of ActivityComponent should be decorated with annotation:
 * [PerActivity]
 */
@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(loginActivity: LoginActivity)

    fun inject(movieActivity: MovieActivity)
}
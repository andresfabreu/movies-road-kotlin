package com.abreu.kotlin_clean.presentation.display_model

class MovieDisplayModel(title: String = "", coverImageUrl: String = "") {
    var title: String? = ""
    var coverImageUrl: String? = ""

    init {
        this.title = title
        this.coverImageUrl = coverImageUrl
    }
}
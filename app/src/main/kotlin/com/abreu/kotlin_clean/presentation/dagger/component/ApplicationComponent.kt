package com.abreu.kotlin_clean.presentation.dagger.component

import android.content.Context
import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import com.abreu.kotlin_clean.domain.executor.PostThreadExecutor
import com.abreu.kotlin_clean.domain.executor.ThreadExecutor
import com.abreu.kotlin_clean.domain.repository.AuthRepositoryProtocol
import com.abreu.kotlin_clean.domain.repository.MovieRepositoryProtocol
import com.abreu.kotlin_clean.presentation.AndroidApplication
import com.abreu.kotlin_clean.presentation.dagger.module.ApplicationModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(androidApplication: AndroidApplication)

    val androidApplication: AndroidApplication

    fun context(): Context

    fun threadExecutor(): ThreadExecutor

    fun postThreadExecutor(): PostThreadExecutor

    fun sessionCache(): SessionCache

    fun authRepositoryProtocol(): AuthRepositoryProtocol

    fun movieRepositoryProtocol(): MovieRepositoryProtocol
}
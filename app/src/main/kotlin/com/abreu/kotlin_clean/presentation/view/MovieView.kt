package com.abreu.kotlin_clean.presentation.view

import com.abreu.kotlin_clean.domain.model.entity.Movie

interface MovieView : BaseView {
    fun setMovies(movies: MutableList<Movie>)
}
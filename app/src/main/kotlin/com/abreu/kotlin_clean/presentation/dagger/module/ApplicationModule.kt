package com.abreu.kotlin_clean.presentation.dagger.module

import android.content.Context
import android.content.SharedPreferences
import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import com.abreu.kotlin_clean.data.mapper.MovieMapper
import com.abreu.kotlin_clean.data.mapper.UserMapper
import com.abreu.kotlin_clean.data.network.api.AuthApi
import com.abreu.kotlin_clean.data.network.api.MovieApi
import com.abreu.kotlin_clean.data.repository.AuthRepository
import com.abreu.kotlin_clean.data.repository.MovieRepository
import com.abreu.kotlin_clean.domain.executor.PostThreadExecutor
import com.abreu.kotlin_clean.domain.executor.ThreadExecutor
import com.abreu.kotlin_clean.domain.repository.AuthRepositoryProtocol
import com.abreu.kotlin_clean.domain.repository.MovieRepositoryProtocol
import com.abreu.kotlin_clean.presentation.AndroidApplication
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: AndroidApplication) {

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return application
    }

    @Provides
    @Singleton
    fun application(): AndroidApplication {
        return application
    }

    @Provides
    @Singleton
    fun provideSubscriberOnThreadExecutor(): ThreadExecutor {
        return ThreadExecutor(Schedulers.io())
    }

    @Provides
    @Singleton
    fun provideObserverOnExecutionThread(): PostThreadExecutor {
        return PostThreadExecutor(AndroidSchedulers.mainThread())
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences {
        return application.getSharedPreferences("app", Context.MODE_APPEND)
    }

    @Provides
    @Singleton
    fun provideSessionCache(sharedPreferences: SharedPreferences): SessionCache {
        return SessionCache(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideAuthRepository(authApi: AuthApi, sessionCache: SessionCache): AuthRepositoryProtocol {
        return AuthRepository(authApi, sessionCache, UserMapper())
    }

    @Provides
    @Singleton
    fun provideMoviesRepository(movieApi: MovieApi): MovieRepositoryProtocol {
        return MovieRepository(movieApi, MovieMapper())
    }

}
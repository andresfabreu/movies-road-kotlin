package com.abreu.kotlin_clean.presentation

import android.app.Application
import com.abreu.kotlin_clean.presentation.dagger.component.ApplicationComponent
import com.abreu.kotlin_clean.presentation.dagger.component.DaggerApplicationComponent
import com.abreu.kotlin_clean.presentation.dagger.module.ApplicationModule

class AndroidApplication : Application() {

    companion object {
        lateinit var component: ApplicationComponent
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

}

package com.abreu.kotlin_clean.presentation.view.activity

import android.os.Bundle
import com.abreu.kotlin_clean.R
import com.abreu.kotlin_clean.presentation.presenter.LoginPresenter
import com.abreu.kotlin_clean.presentation.view.LoginView
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : BaseActivity(), LoginView {

    @Inject lateinit var loginPresenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        component.inject(this)
        loginPresenter.setView(this)

        setupListeners()
    }

    override fun onDestroy() {
        loginPresenter.onDestroy()
        super.onDestroy()
    }

    private fun setupListeners() {
        buttonLogin.setOnClickListener({
            if (editTextUsername.text?.toString().isNullOrBlank()) {
                textInputLayoutUsername.isErrorEnabled = true
                textInputLayoutUsername.error = getString(R.string.invalid_username)
                return@setOnClickListener
            }

            if (editTextPassword.text?.toString().isNullOrBlank()) {
                textInputLayoutPassword.isErrorEnabled = true
                textInputLayoutPassword.error = getString(R.string.invalid_password)
                return@setOnClickListener
            }

            loginPresenter.doLogin("moviesroad", "moviesroad2017")
        })

        editTextUsername.onTextChanged { textInputLayoutUsername.isErrorEnabled = false }

        editTextPassword.onTextChanged { textInputLayoutPassword.isErrorEnabled = false }
    }

}

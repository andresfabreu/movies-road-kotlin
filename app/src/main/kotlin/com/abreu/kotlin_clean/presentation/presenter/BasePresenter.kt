package com.abreu.kotlin_clean.presentation.presenter

import com.abreu.kotlin_clean.presentation.view.BaseView

open class BasePresenter {
    protected var baseView: BaseView? = null

    protected fun showToast(message: String?) {
        baseView?.showToast(message)
    }

    protected fun showLoading() {
        baseView?.showLoading()
    }

    protected fun hideLoading() {
        baseView?.hideLoading()
    }

    open fun <T> setView(view: T) {
        this.baseView = view as BaseView
    }

    open fun onDestroy() {
        baseView = null
    }

    open fun <T> startActivity(classParam: Class<T>) {
        baseView?.startActivity(classParam, null)
    }
}
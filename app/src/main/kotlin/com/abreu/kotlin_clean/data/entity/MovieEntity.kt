package com.abreu.kotlin_clean.data.entity

class MovieEntity {
    var id: Long? = 0L
    var title: String? = ""
    var coverImageUrl: String? = ""
}
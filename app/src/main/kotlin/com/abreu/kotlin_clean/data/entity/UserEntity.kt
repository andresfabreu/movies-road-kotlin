package com.abreu.kotlin_clean.data.entity

class UserEntity {
    var id: Long? = 0L
    var username: String? = ""
    var name: String? = ""
    var role: String? = ""
    var accessToken: String? = ""
}
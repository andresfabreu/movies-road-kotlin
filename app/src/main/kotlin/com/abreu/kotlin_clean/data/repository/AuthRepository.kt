package com.abreu.kotlin_clean.data.repository

import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import com.abreu.kotlin_clean.data.mapper.UserMapper
import com.abreu.kotlin_clean.data.network.api.AuthApi
import com.abreu.kotlin_clean.domain.model.entity.User
import com.abreu.kotlin_clean.domain.repository.AuthRepositoryProtocol
import com.abreu.kotlin_clean.domain.model.request.LoginRequest
import io.reactivex.Flowable
import javax.inject.Inject

class AuthRepository @Inject constructor(private val authApi: AuthApi,
                                         private val sessionCache: SessionCache,
                                         private val userMapper: UserMapper) : AuthRepositoryProtocol {

    override fun doLogin(loginRequest: LoginRequest): Flowable<User> {
        return authApi.doLogin(loginRequest)
                .doOnNext({ sessionCache.setUserEntity(it) })
                .map({ userMapper.transform(it) })
    }
}
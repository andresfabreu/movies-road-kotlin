package com.abreu.kotlin_clean.data.network.service

import com.abreu.kotlin_clean.data.entity.MovieListEntity
import io.reactivex.Flowable
import retrofit2.http.GET


interface MovieService {

    @GET("movie/list")
    fun getMovies(): Flowable<MovieListEntity>
}
package com.abreu.kotlin_clean.data.mapper

import com.abreu.kotlin_clean.data.entity.UserEntity
import com.abreu.kotlin_clean.domain.model.entity.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class UserMapper @Inject constructor() : EntryEntityMapper<User, UserEntity>() {

    override fun transform(entity: UserEntity?): User {
        return User(entity?.name ?: "")
    }
}
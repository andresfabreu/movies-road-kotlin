package com.abreu.kotlin_clean.data.cache.persistent

import android.content.SharedPreferences
import com.abreu.kotlin_clean.data.entity.UserEntity
import com.google.gson.Gson
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class SessionCache @Inject constructor(private val sharedPreferences: SharedPreferences) {

    private val USER_ENTITY_KEY: String = "USER_ENTITY_KEY"

    private val gson: Gson = Gson()

    fun setUserEntity(userEntity: UserEntity) {
        sharedPreferences.edit().putString(USER_ENTITY_KEY, gson.toJson(userEntity)).apply()
    }

    fun getUserEntity() : UserEntity? {
        val userEntityString = sharedPreferences.getString(USER_ENTITY_KEY, null)
        return if(userEntityString == null) null else gson.fromJson(userEntityString, UserEntity::class.java)
    }
}
package com.abreu.kotlin_clean.data.mapper

import com.abreu.kotlin_clean.data.entity.MovieEntity
import com.abreu.kotlin_clean.domain.model.entity.Movie
import javax.inject.Inject
import javax.inject.Singleton

@Singleton class MovieMapper @Inject constructor() : EntryEntityMapper<Movie, MovieEntity>() {

    override fun transform(entity: MovieEntity?): Movie {
        return Movie(entity?.title ?: "", entity?.coverImageUrl ?: "")
    }
}
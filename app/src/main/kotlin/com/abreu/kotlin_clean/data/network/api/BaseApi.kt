package com.abreu.kotlin_clean.data.network.api

import com.abreu.kotlin_clean.BuildConfig
import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import com.abreu.kotlin_clean.data.network.interceptor.SessionInterceptor
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

open class BaseApi {
    fun build(sessionCache: SessionCache): Retrofit {
        val builder = OkHttpClient.Builder().addInterceptor(SessionInterceptor(sessionCache))

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(loggingInterceptor)
        }

        val client = builder.build()

        val retrofitBuilder = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(defaultConverterFactory())

        return retrofitBuilder.build()
    }

    private fun defaultConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create(GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create())
    }

}
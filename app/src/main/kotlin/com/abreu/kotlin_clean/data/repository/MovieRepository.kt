package com.abreu.kotlin_clean.data.repository

import com.abreu.kotlin_clean.data.mapper.MovieMapper
import com.abreu.kotlin_clean.data.network.api.MovieApi
import com.abreu.kotlin_clean.domain.model.entity.Movie
import com.abreu.kotlin_clean.domain.repository.MovieRepositoryProtocol
import io.reactivex.Flowable
import javax.inject.Inject

class MovieRepository @Inject constructor(private val movieApi: MovieApi,
                                          private val movieMapper: MovieMapper) : MovieRepositoryProtocol {

    override fun getMovies(): Flowable<MutableList<Movie>> {
        return movieApi.getMovies().map({ movieMapper.transform(it.movies ?: listOf()) })
    }
}
package com.abreu.kotlin_clean.data.network.api

import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import com.abreu.kotlin_clean.data.entity.MovieListEntity
import com.abreu.kotlin_clean.data.network.service.MovieService
import io.reactivex.Flowable
import javax.inject.Inject

class MovieApi @Inject constructor(sessionCache: SessionCache) : BaseApi() {
    private val movieService: MovieService

    init {
        val retrofit = build(sessionCache)
        movieService = retrofit.create(MovieService::class.java)
    }

    fun getMovies(): Flowable<MovieListEntity> {
        return movieService.getMovies()
    }
}
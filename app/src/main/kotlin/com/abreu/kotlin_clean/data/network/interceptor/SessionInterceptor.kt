package com.abreu.kotlin_clean.data.network.interceptor

import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class SessionInterceptor(private val sessionCache: SessionCache) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val request = createRequest(original)
        return chain.proceed(request)
    }

    private fun createRequest(original: Request): Request {
        val builder = original.newBuilder()
        addHeader(builder, "Authorization", sessionCache.getUserEntity()?.accessToken)
        return builder.build()
    }

    private fun addHeader(builder: Request.Builder?, key: String, value: String?) {
        if (value != null) {
            builder?.addHeader(key, value)
        }
    }
}
package com.abreu.kotlin_clean.data.network.api

import com.abreu.kotlin_clean.data.cache.persistent.SessionCache
import com.abreu.kotlin_clean.data.entity.UserEntity
import com.abreu.kotlin_clean.data.network.service.AuthService
import com.abreu.kotlin_clean.domain.model.request.LoginRequest
import io.reactivex.Flowable
import javax.inject.Inject

class AuthApi @Inject constructor(sessionCache: SessionCache) : BaseApi() {
    private val authService: AuthService

    init {
        val retrofit = build(sessionCache)
        authService = retrofit.create(AuthService::class.java)
    }

    fun doLogin(loginRequest: LoginRequest): Flowable<UserEntity> {
        return authService.doLogin(loginRequest)
    }
}
package com.abreu.kotlin_clean.data.network.service

import com.abreu.kotlin_clean.data.entity.UserEntity
import com.abreu.kotlin_clean.domain.model.request.LoginRequest
import io.reactivex.Flowable
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("auth/login")
    fun doLogin(@Body loginRequest: LoginRequest): Flowable<UserEntity>
}
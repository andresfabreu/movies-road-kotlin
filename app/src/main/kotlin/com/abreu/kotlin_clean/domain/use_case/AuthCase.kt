package com.abreu.kotlin_clean.domain.use_case

import com.abreu.kotlin_clean.domain.executor.PostThreadExecutor
import com.abreu.kotlin_clean.domain.executor.ThreadExecutor
import com.abreu.kotlin_clean.domain.model.entity.User
import com.abreu.kotlin_clean.domain.model.request.LoginRequest
import com.abreu.kotlin_clean.domain.repository.AuthRepositoryProtocol
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

class AuthCase @Inject constructor(subscriberOn: ThreadExecutor,
                                   observerOn: PostThreadExecutor,
                                   private val authRepositoryProtocol: AuthRepositoryProtocol) : UseCase(subscriberOn, observerOn) {

    fun doLogin(loginRequest: LoginRequest, disposableSubscriber: DisposableSubscriber<User>) {
        execute(authRepositoryProtocol.doLogin(loginRequest), disposableSubscriber)
    }
}
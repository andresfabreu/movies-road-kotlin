package com.abreu.kotlin_clean.domain.executor

import io.reactivex.Scheduler

class PostThreadExecutor(val scheduler: Scheduler)
package com.abreu.kotlin_clean.domain.executor

import io.reactivex.Scheduler

class ThreadExecutor(val scheduler: Scheduler)
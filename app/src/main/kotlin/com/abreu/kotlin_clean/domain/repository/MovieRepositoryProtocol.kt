package com.abreu.kotlin_clean.domain.repository

import com.abreu.kotlin_clean.domain.model.entity.Movie
import io.reactivex.Flowable

interface MovieRepositoryProtocol {
    fun getMovies(): Flowable<MutableList<Movie>>
}
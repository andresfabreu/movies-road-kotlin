package com.abreu.kotlin_clean.domain.repository

import com.abreu.kotlin_clean.domain.model.entity.User
import com.abreu.kotlin_clean.domain.model.request.LoginRequest
import io.reactivex.Flowable

interface AuthRepositoryProtocol {
    fun doLogin(loginRequest: LoginRequest): Flowable<User>
}
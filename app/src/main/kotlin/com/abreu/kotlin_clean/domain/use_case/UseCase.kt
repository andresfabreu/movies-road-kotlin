package com.abreu.kotlin_clean.domain.use_case

import com.abreu.kotlin_clean.domain.executor.PostThreadExecutor
import com.abreu.kotlin_clean.domain.executor.ThreadExecutor
import io.reactivex.Flowable
import io.reactivex.disposables.Disposable
import io.reactivex.subscribers.DisposableSubscriber

open class UseCase(protected val subscriberOn: ThreadExecutor, protected val observerOn: PostThreadExecutor) {
    private var disposables: MutableList<Disposable> = mutableListOf()

    fun <T> execute(flowable: Flowable<T>, subscriber: DisposableSubscriber<T>) {
        getDisposable(flowable, subscriber)
    }

    private fun <T> getDisposable(flowable: Flowable<T>, subscriber: DisposableSubscriber<T>): Disposable {
        val disposable = flowable.subscribeOn(subscriberOn.scheduler)
                .observeOn(observerOn.scheduler)
                .subscribeWith(subscriber)

        addDisposable(disposable)

        return disposable

    }

    private fun addDisposable(disposable: Disposable) {
        disposables.removeAll({ it.isDisposed })
        disposables.add(disposable)
    }

    fun disposeAll() {
        disposables.filter({ !it.isDisposed }).forEach({ it.dispose() })
        disposables.clear()
    }
}
package com.abreu.kotlin_clean.domain.model.entity

class Movie(title: String = "", coverImageUrl: String = "") {
    var title: String = ""
    var coverImageUrl: String = ""

    init {
        this.title = title
        this.coverImageUrl = coverImageUrl
    }
}
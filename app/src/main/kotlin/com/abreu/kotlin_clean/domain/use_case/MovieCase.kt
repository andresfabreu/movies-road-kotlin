package com.abreu.kotlin_clean.domain.use_case

import com.abreu.kotlin_clean.domain.executor.PostThreadExecutor
import com.abreu.kotlin_clean.domain.executor.ThreadExecutor
import com.abreu.kotlin_clean.domain.model.entity.Movie
import com.abreu.kotlin_clean.domain.repository.MovieRepositoryProtocol
import io.reactivex.subscribers.DisposableSubscriber
import javax.inject.Inject

class MovieCase @Inject constructor(subscriberOn: ThreadExecutor,
                                    observerOn: PostThreadExecutor,
                                    private val movieRepositoryProtocol: MovieRepositoryProtocol) : UseCase(subscriberOn, observerOn) {

    fun getMovies(disposableSubscriber: DisposableSubscriber<MutableList<Movie>>) {
        execute(movieRepositoryProtocol.getMovies(), disposableSubscriber)
    }
}
package com.abreu.kotlin_clean.domain.model.entity

class User(name: String = "") {
    var name: String? = ""

    init {
        this.name = name
    }
}
package com.abreu.kotlin_clean.domain.model.request

class LoginRequest(username: String = "", password: String = "") {
    var username: String = ""
    var password: String = ""

    init {
        this.username = username
        this.password = password
    }
}